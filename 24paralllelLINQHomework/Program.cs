﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework.Internal;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace _24paralllelLINQHomework
{
    class Program
    {
        const int Count = 1_000_000;
        private static int[] Array = new int[Count];
        
        static void Main(string[] args)
        {
            //ForTest();
            TimeTest(ForTest);
            //await TimeTest(TasksTestAsync);
            TimeTest(ParallelForTest);
            TimeTest(ParallelLinqTest);
            Console.ReadLine();
        }

        static void GenerateArray()
        {
            for (int i = 0; i < Array.Length-1; i++)
            {
                Array[i]= i;
            }
        }

        public static  void TimeTest(Action act)
        {
            GenerateArray();
            //Show(Array);
            long[] timerValueArray = new long[50];
            for (int i = 0; i < 5 ; i++)
            {
                var timer = Stopwatch.StartNew();
                act();
                timer.Stop();
                timerValueArray[i] = timer.ElapsedMilliseconds;
            }            
            Console.WriteLine($"Время: {timerValueArray.Average()} мс.");
            //Show(Array);            
        }

        static void Show(int[] Array)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 15; i++)
            {
                sb.Append(Array[i].ToString() + " ");
            }
            Console.WriteLine(sb);
        }


        [TestMethod()]
        public void ThreadTest()
        {
            Console.WriteLine(Thread.CurrentThread.Name);
            Thread.CurrentThread.Name = "Поток";
            Console.WriteLine(Thread.CurrentThread.Name);
            var thread = new Thread(new ThreadStart(() => Console.WriteLine(Thread.CurrentThread.Name)));
            thread.Start();
        }

        [TestMethod()]
        public static void ForTest()
        {
            TimeTest(() =>
            {
                int sum=0;
                var n = Array.Length;
                for (var i = 0; i < n; i++)
                {
                    sum += Array[i];
                }                
            });
        }

        //[TestMethod()]
        //public void TasksTestAsync()
        //{
        //    List<Task<int>> task = new List<Task<int>>();
        //    var n = Array.Length;
        //    var taskCount = 4;
        //    var arrayPart = Array.Length / taskCount;
        //    for (int i = 0; i < 4; i++)
        //    {
        //        var sum = 0;                
        //        for (int k = 0; k < taskCount; i++)
        //        {
        //            Task myTasks = ArraySum(Array, k * arrayPart, (k * arrayPart) + arrayPart);
        //            int result = myTasks;
        //            sum += result;
        //        }
        //        //task.Add(new Task(() =>
        //        //{
        //        //    var max = i1 * (n + 1) / taskCount;
        //        //    for (int j = i1 * n / taskCount; j < max; j++)
        //        //        sum += Array[i];
        //        //    return sum;
        //        //}));
        //    }

        //    foreach (var t in task)            
        //        t.Start();            
        //    Task.WaitAll(task.ToArray());
        //}

        Task<int> ArraySum(Array myArray, int start, int finish)
        {
            return Task.Run(() =>
                {
                    var sum = 0;
                    for (int i = start; i < finish; i++)
                    {
                        sum += Array[i];
                    }
                    return sum;
                }
            );
        }

        [TestMethod()]
        public static void ParallelForTest()
        {
            int sum=0;
            TimeTest(() =>
            {
                //Parallel.For(0, Array.Length, Calc);       
                ParallelLoopResult result = Parallel.ForEach(Array, (item) =>
                {
                    sum += item;
                });
            });
        }

        [TestMethod()]
        public static  void ParallelLinqTest()
        {
            int sum=0;
            TimeTest(() =>
            {
                Array.AsParallel().ForAll(x => sum += Array[x]); //Sum(x => x);
            });
        }


        //void Calc1(int i) => Array[i] *= 10;
        static void Calc(int i)
        {
            //for (int j = 0; j < 1; j++)
            //{
            //    Array[i] += 1;
            //}
            var sum = Array.Sum(x => x);
            Console.WriteLine(sum.ToString());

            //sum=0;
            //Parallel.For(0, Array.Length, j => sum += Array[j]);
        }
    }
}
